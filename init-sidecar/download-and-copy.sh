#!/bin/sh

mkdir $HOME/.m2/
cp /m2/settings.xml $HOME/.m2/settings.xml

mvn dependency:get -Dartifact=$APP_GAV -Dtransitive=false 
mvn dependency:copy -Dartifact=$APP_GAV -DoutputDirectory=/app-tmp/

cp /app-tmp/*.jar /application/app.jar
