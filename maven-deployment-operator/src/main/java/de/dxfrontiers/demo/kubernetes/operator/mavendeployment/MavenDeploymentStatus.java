package de.dxfrontiers.demo.kubernetes.operator.mavendeployment;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MavenDeploymentStatus {

    private List<MavenDeploymentStatusCondition> conditions = new ArrayList<>();

    private String message;

}
