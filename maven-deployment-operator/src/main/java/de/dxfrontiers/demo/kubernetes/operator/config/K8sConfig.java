package de.dxfrontiers.demo.kubernetes.operator.config;

import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.javaoperatorsdk.operator.Operator;
import io.javaoperatorsdk.operator.api.ResourceController;
import io.javaoperatorsdk.operator.config.runtime.DefaultConfigurationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class K8sConfig {

    @Bean
    public KubernetesClient kubernetesClient() {
        return new DefaultKubernetesClient();
    }

    @Bean(initMethod = "start", destroyMethod = "close")
    public Operator operator(
        KubernetesClient kubernetesClient,
        List<ResourceController> controllers
    ) {
        Operator operator = new Operator(kubernetesClient, DefaultConfigurationService.instance());
        controllers.forEach(operator::register);
        return operator;
    }

}
