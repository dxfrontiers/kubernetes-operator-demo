package de.dxfrontiers.demo.kubernetes.operator.mavendeployment;

import de.dxfrontiers.demo.kubernetes.operator.mavendeployment.MavenDeploymentStatusCondition.Reason;
import de.dxfrontiers.demo.kubernetes.operator.mavendeployment.MavenDeploymentStatusCondition.Status;
import de.dxfrontiers.demo.kubernetes.operator.mavendeployment.MavenDeploymentStatusCondition.Type;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodBuilder;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.NonNamespaceOperation;
import io.fabric8.kubernetes.client.dsl.PodResource;
import io.javaoperatorsdk.operator.api.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
@Controller(generationAwareEventProcessing = false)
@RequiredArgsConstructor
public class MavenDeploymentController implements ResourceController<MavenDeployment> {

    private final KubernetesClient kubernetesClient;

    @Override
    public UpdateControl<MavenDeployment> createOrUpdateResource(MavenDeployment resource, Context<MavenDeployment> context) {

        UpdateControl updateControl = UpdateControl.updateStatusSubResource(resource);

        //check roughly if resource is valid
        if (isValidGav(resource.getSpec().getGav())
                && isValidDnsName(resource.getSpec().getPodName())) {
            try {
                NonNamespaceOperation<Pod, PodList, PodResource<Pod>> pods = kubernetesClient.pods().inNamespace(resource.getMetadata().getNamespace());

                //get existing pod for podName if any
                Optional<Pod> podOptional =
                        pods.list().getItems().stream()
                                .filter(p -> p.getMetadata().getName().equals(resource.getSpec().getPodName()))
                                .findFirst();

                //if no pod for resource is available, create one
                if (podOptional.isEmpty()) {
                    Pod pod = preparePod(resource.getSpec().getPodName(), resource.getSpec().getGav());
                    pods.create(pod);
                    addCondition(resource, createPodCreatedCondition());
                }
                //otherwise update the pod if needed
                else {
                    Pod existingPod = podOptional.get();

                    //if GAV has changed delete the old pod and create a new one
                    if (doesGavNotMatch(existingPod, resource.getSpec().getGav())) {
                        if ((isLastConditionEmpty(resource) || isLastConditionCreated(resource))) {
                            pods.delete(existingPod);
                            addCondition(resource, createPodDeletedCondition());
                        }
                        //if the pod still exists and is marked as being deleted wait a little while.
                        if (isLastConditionPodDeleted(resource)) {
                            //TODO: watch for pod being deleted instead of polling
                            Thread.sleep(100);
                            addCondition(resource, createPodDeletedCondition());
                        }
                    }
                    //if GAV has not changed return "noUpdate"
                    else {
                        updateControl = UpdateControl.noUpdate();
                    }
                }

            } catch (Exception e) {
                //TODO: implement proper handling for exceptions
                log.error("Something bad happened", e);
            }
        } else {
            //TODO: implement proper error handling for invalid GAV
            log.error("Something really bad happened");
        }

        //if GAV has changed return default updateStatusSubResource
        return updateControl;
    }

    @Override
    public DeleteControl deleteResource(MavenDeployment resource, Context<MavenDeployment> context) {
        NonNamespaceOperation<Pod, PodList, PodResource<Pod>> pods = kubernetesClient.pods().inNamespace(resource.getMetadata().getNamespace());

        Optional<Pod> podOptional =
                pods.list().getItems().stream()
                        .filter(p -> p.getMetadata().getName().equals(resource.getSpec().getPodName()))
                        .findFirst();
        if (podOptional.isPresent()) {
            pods.delete(podOptional.get());
            addCondition(resource, createPodDeletedCondition());
        }
        //everything has gone well, resource may be deleted
        return DeleteControl.DEFAULT_DELETE;
    }

    private Pod preparePod(String podName, String gav) {
        //@formatter:off
        return new PodBuilder()
                .withNewMetadata()
                    .withName(podName)
                .endMetadata()
                .withNewSpec()
                    .addNewInitContainer()
                        .withName("init-sidecar-container")
                        .withImage("registry.gitlab.com/dxfrontiers/kubernetes-operator-demo/init-sidecar")
                        .addNewEnv()
                            .withName("APP_GAV")
                            .withValue(gav)
                        .endEnv()
                        .addNewVolumeMount()
                            .withName("application-volume")
                            .withMountPath("/application")
                        .endVolumeMount()
                    .endInitContainer()
                    .addNewContainer()
                        .withName("application-container")
                        .withImage("adoptopenjdk:11-jre")
                        .withCommand("java", "-jar", "/application/app.jar")
                        .addNewVolumeMount()
                            .withName("application-volume")
                            .withMountPath("/application")
                        .endVolumeMount()
                        .addNewPort()
                            .withContainerPort(8080)
                        .endPort()
                    .endContainer()
                    .addNewVolume()
                        .withName("application-volume")
                        .withNewEmptyDir()
                        .endEmptyDir()
                    .endVolume()
                .endSpec()
                .build();
        //@formatter:on
    }

    private boolean isLastConditionPodDeleted(MavenDeployment resource) {
        MavenDeploymentStatusCondition lastCondition = retrieveLastCondition(resource);
        return lastCondition.getReason().equals(Reason.POD_DELETED.getReason());
    }

    private boolean isLastConditionEmpty(MavenDeployment resource) {
        return null == retrieveLastCondition(resource);
    }

    private boolean isLastConditionCreated(MavenDeployment resource) {
        MavenDeploymentStatusCondition lastCondition = retrieveLastCondition(resource);
        return lastCondition.getReason().equals(Reason.POD_CREATED.getReason());
    }

    private MavenDeploymentStatusCondition retrieveLastCondition(MavenDeployment resource) {
        MavenDeploymentStatus status = resource.getStatus();
        if (null == status
                || status.getConditions().isEmpty()) {
            log.debug("Status is null or conditions are empty. Returning null as last condition value.");
            return null;
        }
        List<MavenDeploymentStatusCondition> conditions = status.getConditions();
        return conditions.get(conditions.size() - 1);
    }

    private boolean doesGavNotMatch(Pod pod, String gav) {
        return !pod.getSpec()
                .getInitContainers()
                .get(0)
                .getEnv()
                .get(0)
                .getValue()
                .equals(gav);
    }

    private MavenDeploymentStatusCondition createPodDeletedCondition() {
        return new MavenDeploymentStatusCondition()
                .setMessage("Deleting pod")
                .setLastTransitionTime(retrieveCurrentTimeAsString())
                .setReason(Reason.POD_DELETED.getReason())
                .setType(Type.Pending)
                .setStatus(Status.True);
    }

    private MavenDeploymentStatusCondition createPodCreatedCondition() {
        return new MavenDeploymentStatusCondition()
                .setMessage("Creating Pod")
                .setLastTransitionTime(retrieveCurrentTimeAsString())
                .setReason(Reason.POD_CREATED.getReason())
                .setType(Type.Ready)
                .setStatus(Status.True);
    }

    private String retrieveCurrentTimeAsString() {
        return ZonedDateTime.now()
                .toInstant()
                .atZone(ZoneId.of("Z"))
                .truncatedTo(ChronoUnit.SECONDS)
                .format(DateTimeFormatter.ISO_DATE_TIME);
    }

    private void addCondition(MavenDeployment resource, MavenDeploymentStatusCondition mavenDeploymentStatusCondition) {
        MavenDeploymentStatus status = resource.getStatus();
        if (null == status) {
            resource.setStatus(new MavenDeploymentStatus());
        }
        resource.getStatus()
                .getConditions()
                .add(mavenDeploymentStatusCondition);
    }

    private boolean isValidGav(String gav) {
        String[] gavParts = gav.split(":");
        return gavParts.length == 3
                && !gavParts[0].isBlank()
                && !gavParts[1].isBlank()
                && !gavParts[2].isBlank();
    }

    private boolean isValidDnsName(String uri) {
        //Props go out to Jorge Ferreira: https://stackoverflow.com/questions/106179/regular-expression-to-match-dns-hostname-or-ip-address
        String regex = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$";
        return uri.matches(regex);
    }
}
