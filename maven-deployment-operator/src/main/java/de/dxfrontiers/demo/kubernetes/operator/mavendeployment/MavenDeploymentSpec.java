package de.dxfrontiers.demo.kubernetes.operator.mavendeployment;

import lombok.Data;

@Data
public class MavenDeploymentSpec {

    private String gav;

    private String podName;

}
