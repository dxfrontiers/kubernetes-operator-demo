package de.dxfrontiers.demo.kubernetes.operator.mavendeployment;

import io.fabric8.kubernetes.api.model.Namespaced;
import io.fabric8.kubernetes.client.CustomResource;
import io.fabric8.kubernetes.model.annotation.Group;
import io.fabric8.kubernetes.model.annotation.Kind;
import io.fabric8.kubernetes.model.annotation.Version;

@Group("k8s-demo.dxfrontiers.de")
@Kind("MavenDeployment")
@Version("v1")
public class MavenDeployment extends CustomResource<MavenDeploymentSpec, MavenDeploymentStatus> implements Namespaced {
}
