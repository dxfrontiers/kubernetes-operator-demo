package de.dxfrontiers.demo.kubernetes.operator.mavendeployment;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MavenDeploymentStatusCondition {

    private String lastTransitionTime;

    private String reason;

    private Status status;

    private Type type;

    private String message;

    public enum Type {
        Pending,
        Ready
    }

    public enum Status{
        True,
        False
    }

    @RequiredArgsConstructor
    public enum Reason {
        POD_DELETED("podDeleted"),
        POD_CREATED("podCreated");

        @Getter
        private final String reason;
    }

}
