# Kubernetes Operator Demo

 ## Introduction
  
  This repository contains the example code backing the [2021 JFS Session - Pimp my k8s](https://www.java-forum-stuttgart.de/vortraege/pimp-my-k8s/) talk.

 ## Content

  ### sample-app

   The sample app is the simplest possible Spring Boot Application consisting of an index.html.
   Purpose of this app is to be deployed to a Maven registry to be downloaded by the init container.

  ### init-sidecar

   The init-sidecar is a simple image that downloads a Maven Artifact from the demo-project Maven registry.
   The downloaded jar is then copied to a mounted volume. 
   This sidecar can be used as init container for the pod created by the operator. 

  ### maven-deployment-operator
   The maven-deployment-operator contains the actual operator code.
   The kubernetes-deployment directory contains the custom resource definition (CRD) and an example of the custom resource. 
   The `src` directory contains the java code of the operator. 


 ## Executing the Demo on your local device

  To execute the maven-deployment-operator you need access to a Kubernetes cluster, 
  a local installation of the Kubernetes Commandline Tool (kubectl), and Maven.

  1. log in to your cluster with a high priviliged account (e.g. kubeadmin) 
    (use e.g. [kind](https://kind.sigs.k8s.io/) to setup a local kubernetes cluster)
  2. deploy the CRD to your cluster `kubectl apply -f 10_crd.yml`
  3. start the operator `mvn spring-boot:run`
  4. deploy the sample custom resource to your cluster `kubectl apply -f custom_resource.yml`

  The operator will use your local kubeconfig (kubectl login) to connect to the cluster.


 ## What the Operator deploys
  
  A sample pod like 
  ```yml
apiVersion: v1
kind: Pod
metadata:
  name: demo-pod2 # configurable via custom resource
spec:
  containers:
  - name: application-container
    image: adoptopenjdk:11-jre
    command: ['java', '-jar', '/application/app.jar']
    volumeMounts:
    - mountPath: /application
      name: application-volume
    ports:
    - containerPort: 8080
  initContainers:
  - name: init-sidecar-container
    image: registry.gitlab.com/dxfrontiers/kubernetes-operator-demo/init-sidecar
    env:
    - name: APP_GAV
      value: de.dxfrontiers.demo.kubernetes.operator:sample-app:0.0.2 # configureable via custom resoure
    volumeMounts:
    - mountPath: /application
      name: application-volume
  volumes:
  - name: application-volume
    emptyDir: {}
  ```
